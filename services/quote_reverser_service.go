package services

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gunmurat/quote-reverser/models"
)

type QuoteReverserService interface {
	GetUnReversedQuotes() ([]models.UnReversedModel, error)
	ReversedQuote(quotes []models.UnReversedModel) ([]models.ReversedModel, error)
	Reverse(quote string) (string, error)
	FindSameAuthors(q models.UnReversedModel, quotes []models.UnReversedModel) ([]models.UnReversedModel, error)
	ComposeSameAuthorItems(same []models.UnReversedModel) (models.ReversedModel, error)
	SliceDifference(sames, quotes []models.UnReversedModel) ([]models.UnReversedModel, error)
	Contains(sames []models.UnReversedModel, q models.UnReversedModel) bool
}

type QuoteReverserServiceImpl struct {
	qs []models.ReversedModel
}

func NewQuoteReverserService(qs []models.ReversedModel) QuoteReverserService {
	return &QuoteReverserServiceImpl{
		qs: qs,
	}
}

func (s *QuoteReverserServiceImpl) GetUnReversedQuotes() ([]models.UnReversedModel, error) {

	var quotes []models.UnReversedModel

	resp, err := http.Get("https://type.fit/api/quotes")
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&quotes)
	if err != nil {
		return nil, err
	}

	s.qs = []models.ReversedModel{}

	return quotes, nil
}

func (s *QuoteReverserServiceImpl) Reverse(quote string) (string, error) {

	var str string

	for i := len(quote) - 1; i >= 0; i-- {
		str += string(quote[i])
	}

	return str, nil

}

func (s *QuoteReverserServiceImpl) ReversedQuote(quotes []models.UnReversedModel) ([]models.ReversedModel, error) {

	if len(quotes) > 0 {
		same, err := s.FindSameAuthors(quotes[0], quotes)
		if err != nil {
			return nil, err
		}

		rev, err := s.ComposeSameAuthorItems(same)
		if err != nil {
			return nil, err
		}

		s.qs = append(s.qs, rev)

		diff, err := s.SliceDifference(same, quotes)

		s.ReversedQuote(diff)
	}

	return s.qs, nil
}

func (s *QuoteReverserServiceImpl) FindSameAuthors(q models.UnReversedModel, quotes []models.UnReversedModel) ([]models.UnReversedModel, error) {

	var same []models.UnReversedModel

	for _, v := range quotes {
		if q.Author == v.Author {
			same = append(same, v)
		}
	}

	return same, nil
}

func (s *QuoteReverserServiceImpl) ComposeSameAuthorItems(same []models.UnReversedModel) (models.ReversedModel, error) {

	var rev models.ReversedModel
	var qs []string

	for _, v := range same {

		sr, err := s.Reverse(v.Text)
		if err != nil {
			return rev, err
		}
		qs = append(qs, sr)
	}

	rev = models.ReversedModel{
		Quotes: qs,
		Author: same[0].Author,
	}

	return rev, nil

}

func (s *QuoteReverserServiceImpl) SliceDifference(sames, quotes []models.UnReversedModel) ([]models.UnReversedModel, error) {

	var diff []models.UnReversedModel

	for _, v := range quotes {
		if !s.Contains(sames, v) {
			diff = append(diff, v)
		}
	}

	return diff, nil
}

func (s *QuoteReverserServiceImpl) Contains(sames []models.UnReversedModel, q models.UnReversedModel) bool {

	for _, v := range sames {
		if v.Text == q.Text {
			return true
		}
	}

	return false
}
