package models

type UnReversedModel struct {
	Text   string `json:"text"`
	Author string `json:"author"`
}

type ReversedModel struct {
	Quotes []string `json:"quotes"`
	Author string   `json:"author"`
}
