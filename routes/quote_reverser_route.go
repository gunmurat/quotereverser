package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/quote-reverser/controllers"
	"gitlab.com/gunmurat/quote-reverser/models"
	"gitlab.com/gunmurat/quote-reverser/services"
)

var emp []models.ReversedModel
var controller = controllers.NewQuoteReverserController(services.NewQuoteReverserService(emp))

func QuoteReverserRoute(app *fiber.App) {
	app.Get("/reverse", controller.Reverse)
}
