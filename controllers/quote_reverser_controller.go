package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/gunmurat/quote-reverser/services"
)

type QuoteReverserController interface {
	Reverse(ctx *fiber.Ctx) error
}

type QuoteReverserControllerImpl struct {
	QuoteReverserService services.QuoteReverserService
}

func NewQuoteReverserController(quoteReverserService services.QuoteReverserService) QuoteReverserController {
	return &QuoteReverserControllerImpl{
		QuoteReverserService: quoteReverserService,
	}
}

func (c *QuoteReverserControllerImpl) Reverse(ctx *fiber.Ctx) error {
	quos, errN := c.QuoteReverserService.GetUnReversedQuotes()
	if errN != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error":   errN.Error(),
			"success": false,
		})
	}
	reversedQuotes, err := c.QuoteReverserService.ReversedQuote(quos)
	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"error":   err.Error(),
			"success": false,
		})
	}

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"data":    reversedQuotes,
		"success": true,
	})
}
