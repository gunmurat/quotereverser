package test

import (
	"testing"

	"gitlab.com/gunmurat/quote-reverser/models"
	"gitlab.com/gunmurat/quote-reverser/services"
	"gotest.tools/assert"
)

var testSlice []models.ReversedModel
var service = services.NewQuoteReverserService(testSlice)

func TestReverse(t *testing.T) {
	rev, err := service.Reverse("Hello")
	assert.Equal(t, err, nil)

	assert.Equal(t, "olleH", rev)
}

func TestGetUnReversedData(t *testing.T) {
	quotes, err := service.GetUnReversedQuotes()
	assert.Equal(t, err, nil)

	assert.Check(t, len(quotes) > 0)
}

func TestFindSameAuthors(t *testing.T) {
	quotes, err := service.GetUnReversedQuotes()
	assert.Equal(t, err, nil)

	assert.Check(t, len(quotes) > 0)

	sameAuthors, err := service.FindSameAuthors(quotes[0], quotes)
	assert.Equal(t, err, nil)

	assert.Check(t, len(sameAuthors) > 0)

}

func TestComposeSameAuthorItems(t *testing.T) {
	sames := []models.UnReversedModel{
		{
			Text:   "Hello",
			Author: "Gunmurat",
		},
		{
			Text:   "World",
			Author: "Gunmurat",
		},
	}

	sameAuthorItems, err := service.ComposeSameAuthorItems(sames)

	assert.Equal(t, err, nil)
	assert.Equal(t, len(sameAuthorItems.Quotes), 2)

}

func TestContains(t *testing.T) {
	qs := []models.UnReversedModel{
		{
			Text:   "Hello",
			Author: "Gunmurat",
		},
		{
			Text:   "World",
			Author: "Gunmurat",
		},
	}

	assert.Equal(t, service.Contains(qs, models.UnReversedModel{
		Text:   "Hello",
		Author: "Gunmurat",
	}), true)
}

func TestSliceDifference(t *testing.T) {
	qs := []models.UnReversedModel{
		{
			Text:   "Hello",
			Author: "Gunmurat",
		},
		{
			Text:   "World",
			Author: "Gunmurat",
		},
	}

	q := []models.UnReversedModel{
		{
			Text:   "Hello",
			Author: "Gunmurat",
		},
	}

	diff, err := service.SliceDifference(q, qs)

	assert.Equal(t, err, nil)
	assert.Equal(t, len(diff), 1)
}
